CREATE DATABASE `nextcloud`;
CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY PASSWORD '*622C4A896C7A2B1DEE6B6713116AFF0182BED69F';
GRANT ALL PRIVILEGES ON `nextcloud`.* TO 'nextcloud'@'localhost';

DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;