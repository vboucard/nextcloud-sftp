# dnf install kernel-devel # vboxadditions
dnf update
dnf install -y epel-release yum-utils unzip curl wget lsof bind-utils
dnf install -y bash-completion policycoreutils-python-utils mlocate bzip2
dnf install -y httpd samba-client

cp -f /vagrant/nextcloud.conf /etc/httpd/conf.d/nextcloud.conf

systemctl enable httpd.service
systemctl start httpd.service

# Modules PHP

dnf install -y https://rpms.remirepo.net/enterprise/8/remi/x86_64/remi-release-8.8-1.el8.remi.noarch.rpm
dnf install --enablerepo=remi -y php81 php81-php-gd php81-php-mbstring php81-php-intl php81-php-pecl-apcu\
     php81-php-mysqlnd php81-php-opcache php81-php-pecl-zip php81-php-cli\
     php81-php-ldap php81-php-process php81-php-sodium
dnf install --enablerepo=remi -y php81-php-redis php81-php-pecl-imagick-im7
dnf install -y mariadb mariadb-server
systemctl enable mariadb.service
systemctl start mariadb.service
dnf install -y redis
systemctl enable redis.service
systemctl start redis.service

# wget https://download.nextcloud.com/server/releases/latest-24.tar.bz2
tar xjf /vagrant/latest-24.tar.bz2 -C /var/www/html
# cp -R nextcloud/ /var/www/html/
mkdir /var/www/html/nextcloud/data
chown -R apache:apache /var/www/html/nextcloud
systemctl restart httpd.service
systemctl enable --now firewalld
firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --reload

mkdir /var/www/html/data
touch /var/www/html/data/.ocdata
chown -R apache: /var/www/html/data
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/data(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/data(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/config(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/apps(/.*)?'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.htaccess'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/.user.ini'
semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/nextcloud/3rdparty/aws/aws-sdk-php/src/data/logs(/.*)?'

restorecon -R '/var/www/html/nextcloud/'
restorecon -R '/var/www/html/data/'
setsebool -P httpd_can_network_connect on

# nmcli connection modify enp0s3 ipv4.ignore-auto-dns yes ipv4.dns 192.168.0.10
# systemctl restart NetworkManager


# Kerberos' stuff
# dnf install -y krb5-workstation
# cp -f /vagrant/conf/krb5.conf /etc/krb5.conf
# # cp /vagrant/conf/nextcloud.keytab /etc/nextcloud.keytab
# # kinit -V -k -t /etc/nextcloud.keytab HTTP/nextcloud.lab.lan@LAB.LAN
# ## GSSPROXY
# dnf install -y mod_auth_gssapi gssproxy

# ### configuration pour prendre en compte le keytab
# cp /vagrant/conf/80-httpd.conf /etc/gssproxy/80-httpd.conf
# systemctl restart gssproxy.service
# systemctl enable gssproxy.service

### reconfiguration du service httpd pour prendre en compte le proxy gss
# cp /vagrant/conf/httpd.service /etc/systemd/system/httpd.service
# systemctl daemon-reload

### ajout de la page d'authentification
# cp /vagrant/conf/basic-auth.conf /etc/httpd/conf.d/basic-auth.conf
mv /etc/httpd/conf.d/welcome.conf{,.bak}

systemctl enable --now httpd.service

wget https://github.com/vrana/adminer/releases/download/v4.8.1/adminer-4.8.1-mysql.php\
     -O /var/www/html/adminer.php

# supprimer les fichiers par défaut
# rm -fr /var/www/html/nextcloud/core/skeleton/*

# echo "=================================================="
# echo "Finir l'install ou Faire mysql_secure_installation"
mysql -uroot < /vagrant/mariadb_secure_installation.sql
# CREATE DATABASE `nextcloud`;
# CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY PASSWORD '*5368937F30301F448A08DE1502EA6641F0FB8864';
# GRANT ALL PRIVILEGES ON `nextcloud`.* TO 'nextcloud'@'localhost';

# DELETE FROM mysql.global_priv WHERE User='';
# DELETE FROM mysql.global_priv WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
# DROP DATABASE IF EXISTS test;
# DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
# FLUSH PRIVILEGES;

# mkdir /data
# cp /vagrant/conf/* /data
# chown -R apache: /data
ln -s /bin/php81 /bin/php
echo alias maintenance=\"sudo -u apache php /var/www/html/nextcloud/occ maintenance:mode\" >> /root/.bashrc
echo alias occ=\"sudo -u apache php /var/www/html/nextcloud/occ\" >> /root/.bashrc

occ="sudo -u apache php /var/www/html/nextcloud/occ"

$occ maintenance:install --database='mysql' --database-name='nextcloud' \
                         --database-user='nextcloud' --database-pass='nextcloud' \
                         --admin-user='admin' --admin-pass='password'

apps="firstrunwizard
activity
dashboard
files_trashbin
files_videoplayer
federation
logreeader
nextcoud_annoucements
photos
recommandations
sharebymail
support
survey_client
user_status
weather_status
files_versions"

for app in $apps
do
    $occ app:disable $app
done

$occ config:system:set skeletondirectory
$occ config:system:set default_phone_region --value='FR'
$occ config:system:set has_internet_connection --value=false --type=boolean
$occ config:system:set profile.enabled --value=false --type=boolean
$occ config:system:set datadirectory --value='/var/www/html/data'
$occ config:system:set no_unsupported_browser_warning --value=true --type=boolean
echo "*/5 * * * * /bin/php -f /var/www/html/nextcloud/cron.php" |crontab -u apache -